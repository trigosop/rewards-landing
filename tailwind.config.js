const defaultTheme = require('tailwindcss/defaultTheme')
const plugin = require('tailwindcss/plugin')

module.exports = {
  mode: 'jit',
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        main: '#5E00D7',
        secondary: '#6F6B75',
        title: '#332D3B',
        'violet-100': '#F1E7FF',
        'violet-700': '#430099',
        'grey-black': '#332D3B',
      },
      backgroundSize: {
        full: '100% 100%',
      },
      scale: {
        25: '.25',
      },
      screens: {
        xs: '475px',
        ...defaultTheme.screens,
        '4xl': '1920px',
      },
      fontSize: {
        '3.5xl': [
          '2rem',
          {
            lineHeight: '40px',
          },
        ],
        '5.5xl': [
          '3.25rem',
          {
            lineHeight: '56px',
          },
        ],
      },
    },
    fontFamily: {
      body: ['Inter'],
      display: ['"Museo Sans Rounded"'],
    },
    container: {
      center: true,
      padding: '1rem',
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    plugin(function ({ addBase, theme }) {
      addBase({
        body: {
          fontFamily: theme('fontFamily.body'),
          color: theme('colors.secondary'),
        },
        h1: {
          color: theme('colors.title'),
        },
        h2: {
          color: theme('colors.title'),
        },
        h3: {
          color: theme('colors.title'),
        },
        h4: {
          color: theme('colors.title'),
        },
        h5: {
          color: theme('colors.title'),
        },
        h6: {
          color: theme('colors.title'),
        },
      })
    }),
  ],
}
